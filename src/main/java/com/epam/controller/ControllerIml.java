package com.epam.controller;

import com.epam.model.Model;
import com.epam.model.TestLogic;

/**
 * Class contains methods for proper working of controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ControllerIml implements Controller {

    private Model link;

    public ControllerIml() {
        link = new TestLogic();
    }


    @Override
    public String getTrace() {
        return link.getTrace();
    }

    @Override
    public String getDebug() {
        return link.getDebug();
    }

    @Override
    public String getWarning() {
        return link.getWarning();
    }

    @Override
    public String getError() {
        return link.getError();
    }

    @Override
    public String getFatal() {
        return link.getFatal();
    }
}

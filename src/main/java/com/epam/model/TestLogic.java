package com.epam.model;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class generate all the Loggers in following program.
 * Created by Borys Latyk on 21/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */

public class TestLogic implements Model {
    private static Logger logger1 = LogManager.getLogger(TestLogic.class);
    @Override
    public String getTrace() {
        logger1.trace("Trace ok");
        return "Test logger for trace is ok";
    }

    @Override
    public String getDebug() {
        logger1.debug("Debug ok");
        return "Test logger for debug is ok";
    }

    @Override
    public String getWarning() {
        logger1.warn("Warning ok");
        return "Test logger for warning is ok";
    }

    @Override
    public String getError() {
        logger1.error("Error ok");
        return "Test logger for error is ok";
    }

    @Override
    public String getFatal() {
        new ExampleSMS();
        logger1.fatal("Fatal is ok");
        return "Test logger for fatal is ok";
    }
}

package com.epam.model;

/**
 * Interface for Model.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public interface Model {
    String getTrace();

    String getDebug();

    String getWarning();

    String getError();

    String getFatal();
}
